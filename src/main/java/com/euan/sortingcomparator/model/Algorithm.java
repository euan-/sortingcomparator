package com.euan.sortingcomparator.model;

import com.google.common.collect.Ordering;

import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

abstract public class Algorithm implements SortingAlgorithm {

    private String name;
    private String complexity;
    private List<Integer> itemsToSort;
    private List<Integer> sortedList = new ArrayList<>();
    private long timeElapsed;

    protected Algorithm(String name, String averageComplexity) {
        this.name = name;
        this.complexity = averageComplexity;
    }

    public String getName() {
        return name;
    }

    public String getComplexity() {
        return complexity;
    }

    protected List<Integer> getItemsToSort() {
        return itemsToSort;
    }

    protected List<Integer> getSortedList() {
        return sortedList;
    }

    public void setItemsToSort(List<Integer> itemsToSort) {
        this.itemsToSort = itemsToSort;
    }

    public long getTimeElapsed() {
        return timeElapsed;
    }

    protected void addToSortedList(Integer integer) {
        sortedList.add(integer);
    }

    protected void addToSortedListAtIndex(Integer integer, int index) {
        sortedList.add(index, integer);
    }

    protected void addToSortedList(List<Integer> integers) {
        sortedList.addAll(integers);
    }

    protected void removeFromItemsToSort(Integer integer) {
        itemsToSort.remove(integer);
    }

    public Boolean sortingComplete() {
        return sortedList.size() > 0 && Ordering.natural().isOrdered(sortedList);
    }

    @Override
    public void sort() {
        Instant start = Instant.now();
        performSort();
        Instant finish = Instant.now();

        timeElapsed = Duration.between(start, finish).toNanos();

        assert sortingComplete();
    }

    protected abstract void performSort();

}