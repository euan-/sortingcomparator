package com.euan.sortingcomparator.model;

public interface SortingAlgorithm {

    void sort();

    Boolean sortingComplete();

}