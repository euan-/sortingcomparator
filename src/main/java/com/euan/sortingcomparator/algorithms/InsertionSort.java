package com.euan.sortingcomparator.algorithms;

import com.euan.sortingcomparator.model.Algorithm;

public class InsertionSort extends Algorithm {

    public InsertionSort() {
        super("Insertion Sort", "О(n^2)");
    }

    protected void performSort() {
        getItemsToSort().forEach(a -> {
            int index = findIndexToInsert(a);
            addToSortedListAtIndex(a, index);
        });
    }

    private int findIndexToInsert(Integer integer) {
        int indexToAddAt = 0;

        for (int i = 0; i < getSortedList().size(); i++) {
            if (integer >= getSortedList().get(i)) indexToAddAt = i + 1;
        }

        return indexToAddAt;
    }


}