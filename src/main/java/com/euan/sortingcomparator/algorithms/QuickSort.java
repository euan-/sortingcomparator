package com.euan.sortingcomparator.algorithms;

import com.euan.sortingcomparator.model.Algorithm;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class QuickSort extends Algorithm {

    public QuickSort() {
        super("Quick Sort", "О(nlogn)");
    }

    protected void performSort() {
        int[] itemsToSort = getItemsToSort().stream().mapToInt(a -> a).toArray();

        quickSort(itemsToSort, 0, itemsToSort.length - 1);

        addToSortedList(Arrays.stream(itemsToSort).boxed().collect(Collectors.toList()));
    }

    private void quickSort(int[] itemsToSort, int begin, int end) {
        if (begin < end) {
            int partitionIndex = partition(itemsToSort, begin, end);

            quickSort(itemsToSort, begin, partitionIndex - 1);
            quickSort(itemsToSort, partitionIndex + 1, end);
        }
    }

    // Moves all elements <= pivot (last element) to the left of pivot
    private int partition(int[] itemsToSort, int begin, int end) {
        int pivot = itemsToSort[end];
        int i = begin - 1;

        for (int j = begin; j < end; j++) {
            if (itemsToSort[j] < pivot) {
                i++;

                int swapTemp = itemsToSort[i];
                itemsToSort[i] = itemsToSort[j];
                itemsToSort[j] = swapTemp;
            }
        }

        int swapTemp = itemsToSort[i+1];
        itemsToSort[i+1] = itemsToSort[end];
        itemsToSort[end] = swapTemp;

        return i + 1;
    }

    private void quickSortList(List<Integer> listToSort, int startIndex, int endIndex) {
        if (startIndex < endIndex) {
            int partitionIndex = partitionList(listToSort, startIndex, endIndex);

            quickSortList(listToSort, startIndex, partitionIndex - 1);
            quickSortList(listToSort, partitionIndex + 1, endIndex);
        }
    }

    // Moves all elements <= pivot (last element) to the left of pivot
    private int partitionList(List<Integer> listToSort, int begin, int end) {
        int pivot = listToSort.get(end);
        int i = begin - 1;

        for (int j = begin; j < end; j++) {
            if (listToSort.get(j) <= pivot) {
                i++;

                Collections.swap(listToSort, i, j);
            }
        }

        Collections.swap(listToSort, i + 1, end);

        return i + 1;
    }
}