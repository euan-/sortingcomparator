package com.euan.sortingcomparator.algorithms;

import com.euan.sortingcomparator.model.Algorithm;

import java.util.Collections;

public class BubbleSort extends Algorithm {

    public BubbleSort() {
        super("Bubble Sort", "О(n^2)");
    }

    protected void performSort() {
        boolean swapPerformed = true;

        while (swapPerformed) {
            swapPerformed = false;

            for (int i = 0; i < getItemsToSort().size() - 1; i++) {
                int j = i + 1;

                Integer left = getItemsToSort().get(i);
                Integer right = getItemsToSort().get(j);

                if (left > right) {
                    swapPerformed = true;
                    Collections.swap(getItemsToSort(), i, j);
                }
            }
        }

        addToSortedList(getItemsToSort());
    }

}