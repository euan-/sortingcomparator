package com.euan.sortingcomparator.algorithms;

import com.euan.sortingcomparator.model.Algorithm;

import java.util.Comparator;

public class JavaSort extends Algorithm {

    public JavaSort() {
        super("Java Sort (Timsort)", "О(nlogn)");
    }

    protected void performSort() {
        getItemsToSort().sort(Comparator.naturalOrder());
        addToSortedList(getItemsToSort());
    }

}