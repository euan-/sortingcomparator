package com.euan.sortingcomparator.algorithms;

import com.euan.sortingcomparator.model.Algorithm;

public class SelectionSort extends Algorithm {

    public SelectionSort() {
        super("Selection Sort", "О(n^2)");
    }

    protected void performSort() {
        while (getItemsToSort().size() > 0) {
            Integer integer = findSmallestInteger();
            addToSortedList(integer);
            removeFromItemsToSort(integer);
        }
    }

    private Integer findSmallestInteger() {
        Integer smallestInteger = Integer.MAX_VALUE;

        for (Integer integer: getItemsToSort()) {
            if (smallestInteger > integer) smallestInteger = integer;
        }

        return smallestInteger;
    }

}