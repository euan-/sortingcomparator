package com.euan.sortingcomparator;

import java.util.Arrays;
import java.util.List;
import java.util.SplittableRandom;
import java.util.stream.Collectors;

abstract public class GeneratorHelper {

    int[] goodSortingNumberOfEntriesToTest = new int[] {10, 100, 1000, 10000, 100000, 1000000};
    protected int[] badSortingNumberOfEntriesToTest = new int[] {10, 100, 1000, 10000, 15000};

    List<Integer> generateData(int numberOfRandomItems) {
        int[] randomArray = new SplittableRandom().ints(
                numberOfRandomItems, 0, Integer.MAX_VALUE).parallel().toArray();

        return Arrays.stream(randomArray).boxed().collect(Collectors.toList());
    }

}