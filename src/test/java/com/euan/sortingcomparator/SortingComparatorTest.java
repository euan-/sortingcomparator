package com.euan.sortingcomparator;

import com.euan.sortingcomparator.algorithms.*;
import com.euan.sortingcomparator.model.Algorithm;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.google.common.collect.Streams.zip;

class SortingComparatorTest extends GeneratorHelper {

    @Test
    void compareSortingAlgorithms() {
        Stream<String> algorithmOrderForEachRun =
                Arrays.stream(badSortingNumberOfEntriesToTest).boxed().map(numberOfEntries -> {
                    List<Integer> data = generateData(numberOfEntries);

                    List<Algorithm> sortingAlgorithms = Arrays.asList(
                            new BubbleSort(), new InsertionSort(), new JavaSort(), new QuickSort(), new SelectionSort());

                    sortingAlgorithms.forEach(algo -> {
                        algo.setItemsToSort(data);
                        algo.sort();
                    });

                    sortingAlgorithms.sort(Comparator.comparing(Algorithm::getTimeElapsed));
                    return sortingAlgorithms.stream().map(Algorithm::getName).collect(Collectors.joining(","));
                });

        List<String> output =
                zip(algorithmOrderForEachRun, Arrays.stream(badSortingNumberOfEntriesToTest).boxed(),
                        (String algorithmOrder, Integer entries) -> "Result for " + entries + " entries : " + algorithmOrder)
                .collect(Collectors.toList());

        output.forEach(System.out::println);
    }

}