package com.euan.sortingcomparator;

import com.euan.sortingcomparator.model.Algorithm;
import org.junit.jupiter.api.Test;

import java.util.*;
import java.util.stream.IntStream;

abstract public class SortingTest extends GeneratorHelper {

    protected Algorithm algorithm;
    protected int[] entrySizeToTest;

    @Test
    void sortingTest() {
        setEntrySizesToTest();

        System.out.println("Entries, Avg Time Taken (nanos), Total Time Taken (nanos)");

        Arrays.stream(entrySizeToTest).forEach(numberOfEntries -> {
            int numberOfRuns = 5;

            long totalTimeTaken = IntStream.range(0, numberOfRuns).boxed().mapToLong(a -> {
                setAlgorithm();
                algorithm.setItemsToSort(generateData(numberOfEntries));
                algorithm.sort();

                return algorithm.getTimeElapsed();
            }).sum();

            long averageTimeTaken = totalTimeTaken / numberOfEntries;

            System.out.println(numberOfEntries + "," + averageTimeTaken + "," + totalTimeTaken);
        });
    }

    protected abstract void setAlgorithm();

    protected void setEntrySizesToTest() {
        entrySizeToTest = goodSortingNumberOfEntriesToTest;
    }

}