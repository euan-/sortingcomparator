package com.euan.sortingcomparator.algorithms;

import com.euan.sortingcomparator.SortingTest;

class InsertionSortTest extends SortingTest {

    @Override
    protected void setAlgorithm() {
        algorithm = new InsertionSort();
    }

    @Override
    protected void setEntrySizesToTest() {
        entrySizeToTest = badSortingNumberOfEntriesToTest;
    }

}