package com.euan.sortingcomparator.algorithms;

import com.euan.sortingcomparator.SortingTest;

class BubbleSortTest extends SortingTest {

    @Override
    protected void setAlgorithm() {
        algorithm = new BubbleSort();
    }

    @Override
    protected void setEntrySizesToTest() {
        entrySizeToTest = badSortingNumberOfEntriesToTest;
    }

}