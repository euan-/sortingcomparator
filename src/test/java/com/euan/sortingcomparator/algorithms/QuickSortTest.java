package com.euan.sortingcomparator.algorithms;

import com.euan.sortingcomparator.SortingTest;

class QuickSortTest extends SortingTest {

    @Override
    protected void setAlgorithm() {
        algorithm = new QuickSort();
    }

}