package com.euan.sortingcomparator.algorithms;

import com.euan.sortingcomparator.SortingTest;

class JavaSortTest extends SortingTest {

    @Override
    protected void setAlgorithm() {
        algorithm = new JavaSort();
    }

}