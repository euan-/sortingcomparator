package com.euan.sortingcomparator.algorithms;

import com.euan.sortingcomparator.SortingTest;

class SelectionSortTest extends SortingTest {

    @Override
    protected void setAlgorithm() {
        algorithm = new SelectionSort();
    }

    @Override
    protected void setEntrySizesToTest() {
        entrySizeToTest = badSortingNumberOfEntriesToTest;
    }

}