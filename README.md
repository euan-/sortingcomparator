# Sorting Comparator

Project that implements and compares various sorting algorithms, written in Java.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for playing.

### Installing

Clone the repo and switch to master branch on your local machine

```
git clone https://USERNAME@bitbucket.org/euan-/sortingcomparator.git
```

Import as a pom project in IntelliJ, reimport and run a clean install

## Sorting Algorithms

1. Bubble Sort - О(n^2)

    Simple sorting algorithm that repeatedly steps through the list, compares adjacent pairs and swaps them if they are in the wrong order. 
    
    The pass through the list is repeated until the list is sorted i.e. no swaps are performed in the pass through.
    
2. Insertion Sort - О(n^2)

    Simple sorting algorithm that builds the final sorted array (or list) one item at a time by inserting them into the correct position in the sorted list.

3. Java Sort (Timsort) - О(nlogn)

    Finds subsequences of the data that are already ordered, and uses that knowledge to sort the remainder more efficiently. 
    
    This is done by merging an identified subsequence, called a run, with existing runs until certain criteria are fulfilled.

4. Quick Sort - О(nlogn)

    A divide and conquer algorithm in which a large array is divided into two smaller sub-arrays: the low elements and the high elements. 
    
    Quicksort can then recursively sort the sub-arrays. 
    
    The steps are :
    
    * Pick an element, called a pivot, from the array.
    * Partitioning: reorder the array so that all elements with values less than the pivot come before the pivot, while all elements with values greater than the pivot come after it (equal values can go either way). After this partitioning, the pivot is in its final position. This is called the partition operation.
    * Recursively apply the above steps to the sub-array of elements with smaller values and separately to the sub-array of elements with greater values.
    
5. Selection Sort - О(n^2)

    Builds up a sorted list by finding the smallest value in the unsorted list and adding it to the end of the sorted list.

## Compare the algorithms

You can run SortingComparatorTest to compare the performance of the algorithms.

## Running the tests

Simply run a clean install on the root pom to run all tests

```
mvn clean install
```

## Built With

* [Java 10.0.1](https://www.oracle.com/technetwork/java/javase/10-0-1-relnotes-4308875.html) - Development language
* [Maven](https://maven.apache.org/) - Dependency Management

## Authors

* **Euan Maciver** - *Initial build* - 20190224 - [euan-](https://bitbucket.org/euan-/)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details